__author__ = 'mel'

from random import randint

class Game():
    # Holds game data
    def __init__(self):
        self.word, self.found_so_far = generate_word() # found_so_far only shows revealed letters, starts with blanks
        #print(self.word, "".join(self.found_so_far))
        self.lives = 7
        #self.guessed_letters = [] # Decided to get rid of it and just hang them instead

    def guess_letter(self):
        print("".join(self.found_so_far))
        inp = input("Guess a letter: ")
        correct = False # if you guess a letter, you won't lose a life
        for i, letter in enumerate(self.word):   # checks if you guessed a correct letter and gets index
            if inp == letter:
                correct = True
                self.found_so_far[i]=letter # reveal guessed letters in word
        if correct == False:
            self.lose_life()

    def lose_life(self): # reduce life count and draw current status
        self.lives -= 1
        print_gallows(self.lives)

def generate_word():
    wordlist = ["bird","plane","bee"]
    word = wordlist[randint(0,2)]
    return word, list("-"*len(word)) # return found_so_far as a list so it can be changed later

def print_gallows(lives):
    drawings = {0 : "  ____\n   |  |\n   O  |\n  \|/ |\n   |  |\n  / \ |\n______|",
                1 : "  ____\n   |  |\n   O  |\n  \|/ |\n   |  |\n  /   |\n______|",
                2 : "  ____\n   |  |\n   O  |\n  \|/ |\n   |  |\n      |\n______|",
                3 : "  ____\n   |  |\n   O  |\n  \|/ |\n      |\n      |\n______|",
                4 : "  ____\n   |  |\n   O  |\n  \|  |\n      |\n      |\n______|",
                5 : "  ____\n   |  |\n   O  |\n   |  |\n      |\n      |\n______|",
                6 : "  ____\n   |  |\n   O  |\n      |\n      |\n      |\n______|",
                7 : "  ____\n   |  |\n      |\n      |\n      |\n      |\n______|",
    }
    print(drawings[lives])
    return

def main():
    mygame = Game()
    while(True):
        mygame.guess_letter()
        if mygame.found_so_far == list(mygame.word):
            print(mygame.word)
            print("You win!")
            return
        if mygame.lives == 0:
            print("You got hanged!")
            return
    return

if __name__ == '__main__':
    main()

